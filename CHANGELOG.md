# CHANGELOG

## Unreleased

## 1.0.1 - 2015.03.10

- Fix lexer finding logic [#1](https://github.com/JuanitoFatas/html-pipeline-rouge_filter/pull/1) by @JuanitoFatas

## 1.0.0 - 2015.02.08

- RougeFilter (Implementation reference: [SyntaxHighlightFilter](https://github.com/jch/html-pipeline/blob/956be5bf3d3b0dbe47dd9215b85b56773f23efb1/lib/html/pipeline/syntax_highlight_filter.rb)).
